package edu.epam.izhevsk.junit;

public class AccountPaymentController {
  private PaymentController paymentController;
  private long userId;
  
  AccountPaymentController(long userId, AccountService accountService, DepositService depositService) {
    paymentController = new PaymentController(accountService, depositService);
    this.userId = userId;
  }
  
  void deposit(Long amount) throws InsufficientFundsException {
    paymentController.deposit(amount, userId);
  }
}
