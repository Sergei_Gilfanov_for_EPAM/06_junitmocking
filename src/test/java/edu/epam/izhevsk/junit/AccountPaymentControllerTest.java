package edu.epam.izhevsk.junit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import static org.mockito.AdditionalMatchers.lt;
import static org.mockito.AdditionalMatchers.geq;

@RunWith(MockitoJUnitRunner.class)
public class AccountPaymentControllerTest {
  @Mock private AccountService accountService;
  @Mock private DepositService depositService;
  private AccountPaymentController accountPaymentController;
  
  @Before
  public void setUp() throws InsufficientFundsException {
    when(accountService.isUserAuthenticated((long)100)).thenReturn(true);
    when(depositService.deposit(lt(100L), any(Long.class))).thenReturn("Done");
    when(depositService.deposit(geq(100L), any(Long.class))).thenThrow(new InsufficientFundsException());
  }

  private void deposit(long amount, long userid) throws InsufficientFundsException {
    accountPaymentController = new AccountPaymentController(userid, accountService, depositService);
    accountPaymentController.deposit(amount);
  }
  
  @Test
  public void authecatedSmallDepositShouldWork() throws InsufficientFundsException {
    long amount = 50L;
    long userid = 100L;
    deposit(amount, userid);
    verify(accountService).isUserAuthenticated(Matchers.eq(userid));
  }

  @Test
  public void upperLimitOfSmallDeposisShouldWork() throws InsufficientFundsException {
    long amount = 99L;
    long userid = 100L;
    deposit(amount, userid);
    verify(accountService).isUserAuthenticated(Matchers.eq(userid));
  }
  
  @Test(expected=SecurityException.class)
  public void unauthenticatedUserShouldFall() throws InsufficientFundsException {
    long amount = 50L;
    long userid = 101L;
    deposit(amount, userid);
 }
  
  @Test(expected=InsufficientFundsException.class)
  public void bigDepositShouldFall() throws InsufficientFundsException {
    long amount = 10000L;
    long userid = 100L;
    deposit(amount, userid);
  }
  
  @Test(expected=InsufficientFundsException.class)
  public void lowerLimitOfBigDepositsShouldFall() throws InsufficientFundsException {
    long amount = 100L;
    long userid = 100L;
    deposit(amount, userid);
  }
}

