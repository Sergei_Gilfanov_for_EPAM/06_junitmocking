package edu.epam.izhevsk.junit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import static org.mockito.AdditionalMatchers.lt;
import static org.mockito.AdditionalMatchers.geq;

@RunWith(MockitoJUnitRunner.class)
public class PaymentControllerTest {
  @Mock private AccountService accountService;
  @Mock private DepositService depositService;
  @InjectMocks PaymentController paymentController;
  
  @Before
  public void setUp() throws InsufficientFundsException {
    when(accountService.isUserAuthenticated(100L)).thenReturn(true);
    when(depositService.deposit(lt(100L), any(Long.class))).thenReturn("Done");
    when(depositService.deposit(geq(100L), any(Long.class))).thenThrow(new InsufficientFundsException());
  }

  @Test
  public void authecatedSmallDepositShouldWork() throws InsufficientFundsException {
    paymentController.deposit(50L, 100L);
    verify(accountService).isUserAuthenticated(Matchers.eq(100L));
  }

  @Test
  public void upperLimitOfSmallDeposisShouldWork() throws InsufficientFundsException {
    long amount = 99L;
    long userid = 100L;
    paymentController.deposit(amount, userid);
    verify(accountService).isUserAuthenticated(Matchers.eq(userid));
  }
  
  @Test(expected=SecurityException.class)
  public void unauthenticatedUserShouldFall() throws InsufficientFundsException {
    long amount = 50L;
    long userid = 101L;
    paymentController.deposit(amount, userid);
  }
  
  @Test(expected=InsufficientFundsException.class)
  public void bigDepositShouldFall() throws InsufficientFundsException {
    long amount = 10000L;
    long userid = 100L;
    paymentController.deposit(amount, userid);
  }
  
  @Test(expected=InsufficientFundsException.class)
  public void lowerLimitOfBigDepositsShouldFall() throws InsufficientFundsException {
    long amount = 100L;
    long userid = 100L;
    paymentController.deposit(amount, userid);
  }
}

